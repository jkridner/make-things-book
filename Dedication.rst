Dedication
##########

To my son, now 8, and Cassie, Travis, and Mikey and Katie, because I
love seeing you find the joy in learning and how it continues to
open the door to what we have yet to discover.

To Bill, Ken and Jay, because I still don't understand the spaces between friends.

To Billye and Aubrey, because we are only realized through the connections we make with others.

To Alan, Mark and Jon, because knowing what you don't know is hard, and figuring out
what to do with that lack of knowledge is pretty much impossible.

To Tony, because your vision of a better world is helping me reconnect with my childhood self.

To my wife, because together we can take over the world. Er, I mean make it a better place.

To my father, because a more kind and generous man never lived.

To my mother, because I want to give back to the world what you gave to me.
