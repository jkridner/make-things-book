.. _schools-of-thought:

Definitions of various schools of thought
=========================================

.. _thought-mathematics:

Mathematics
-----------

.. _thought-science:

Science
-------

.. _definition-science:

Definition
~~~~~~~~~~

Fundamentally defined by the scientific method of observation,
hypothesis, experimentation and communication, science seeks to better
understand causality.

.. _lessons-science:

Lessons
~~~~~~~

-  If it is not possible to devise an experiment in an attempt to prove
   a theory is wrong, then it isn't a good scientific theory.

.. _thought-physics:

Physics
-------

.. _thought-engineering:

Engineering
-----------

.. _definition-engineering:

Definition
~~~~~~~~~~

Apply science and mathematics to create things that help solve human
problems, retaining knowledge from previous applications that can be
applied in future attempts to create more things.

.. _lessons-engineering:

Lessons
~~~~~~~

-  Failure is a critical part of the engineering process.
-  While engineering practice is largely limited to working with
   physicial things, engineering techniques can be applied in many
   disciplines where there are repeatable processes that can be
   improved.

.. _thought-philosophy:

Philosophy
----------

.. _thought-political-economics:

Political economics
-------------------
