..
   Kid Steps to Making Things Beautiful

.. _make-things-book:

Index
=====

See https://git.beagleboard.org/jkridner/make-things-book for the source.

.. toctree::

   README
   Letter-to-Andy
   Dedication
   Foreward
   Definitions_and_lessons
   History
   Least-Necessary-to-Know-About-Computers
   Failure_modes
   projects/90-degree-angles/README

